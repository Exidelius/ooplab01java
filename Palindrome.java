public class Palindrome {
    public static void main(String[] args) 
    {
        // String s = System.console().readLine().toString();
        // while(!s.equals(""))
        // {
        //     System.out.println(reverseString(s));
        //     if (isPalindrome(s)) System.out.println(s + " is Palindrome");
        //     else System.out.println(s + " isn't Palindrome");
        //     s = System.console().readLine();
        // }
        for (int i = 0; i < args.length; i++) {
            String s = args[i].toString();
            System.out.println(reverseString(s));
            if (isPalindrome(s)) System.out.println(s + " is Palindrome");
            else System.out.println(s + " isn't Palindrome");
        }
    }

    public static String reverseString(String s)
    {
        //StringBuilder temp = new StringBuilder(s);
        //return temp.reverse().toString();
        StringBuilder temp = new StringBuilder("");
        for (int i = 0; i < s.length(); i++) {
            temp.append(s.charAt(s.length() -1 -i));
        }
        return temp.toString(); 
    }

    public static boolean isPalindrome(String s)
    {
        return (s.equals(reverseString(s)));
    }

}